﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TylsaaHappoo : MonoBehaviour
{
    public ComputeShader happoCS;
    public Material rendausMaterial;
    private RenderTexture rendausTex;
    private const uint renduasTexSizeX = 1920 / 10;
    private const uint renduasTexSizeY = 1080 / 10;
    public static float happoisuus = 0.4f; // maximi happoisuus: 1, terveys: 0
    public static float syvyys = 1.0f; // maksimisyvyys: 1, pinta: 0

    void Start()
    {
        rendausTex = new CustomRenderTexture((int)(renduasTexSizeX), (int)(renduasTexSizeY),
            RenderTextureFormat.ARGB32, RenderTextureReadWrite.Linear);

        rendausTex.enableRandomWrite = true;

    }

    void Update()
    {
        int kernelIndex = happoCS.FindKernel("CSMain");
        happoCS.SetTexture(kernelIndex, "Result", rendausTex);
        happoCS.SetFloat("Time", Time.time);
        happoCS.SetFloat("happoisuus", happoisuus);
        happoCS.SetFloat("syvyys", syvyys);

        uint threadGroupSizeX, threadGroupSizeY, threadGroupSizeZ;
        happoCS.GetKernelThreadGroupSizes(kernelIndex, out threadGroupSizeX, out threadGroupSizeY, out threadGroupSizeZ);

        int dispatchX = (int)(renduasTexSizeX / threadGroupSizeX);
        int dispatchY = (int)(renduasTexSizeY / threadGroupSizeY);
        int dispatchZ = (int)(1);

        happoCS.Dispatch(kernelIndex, dispatchX, dispatchY, dispatchZ);


        rendausMaterial.mainTexture = rendausTex;
    }
}
