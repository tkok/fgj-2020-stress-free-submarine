﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneNuuskutin : MonoBehaviour
{
	public Camera cam;

	void Awake()
	{
		UnityEngine.SceneManagement.SceneManager.LoadScene("Submarine",
			UnityEngine.SceneManagement.LoadSceneMode.Additive);
	}

	// Start is called before the first frame update
	void Start()
	{
		cam = GameObject.Find("SubmarineCam").GetComponent<Camera>();
		//cam.enabled = false;
		cam.cullingMask = LayerMask.GetMask("UI");
	}

	// Update is called once per frame
	void Update()
	{
		
	}

	public void getVisible(out float camFov, out float camNear, out float camFar, uint shapeCountMax
		, ref List<SDFBox> boxes, ref List<SDFSphere> spheres, ref List<SphereLight> lights, ref List<SDFMissionStuff> missionStuffs)
	{
		Vector3 camPos = cam.transform.position;
		Quaternion camRot = cam.transform.rotation;
		Quaternion camRotInv = Quaternion.Inverse(camRot);

		camFov = Mathf.Deg2Rad * cam.fieldOfView;
		camNear = cam.nearClipPlane;
		camFar = cam.farClipPlane;
	
		Vector3 camFwd = camRot * new Vector3(0, 0, 1);

		List<Transform> boxTrans = new List<Transform>();

		// Shapes
		{
			Object[] objs = FindObjectsOfType<MeshFilter>();
			for (uint i = 0; i < objs.Length; ++i)
			{
				MeshFilter meshFilter = (MeshFilter)objs[i];
				Transform trans = meshFilter.transform;

				bool cylinderOrCube = meshFilter.sharedMesh.name == "Cylinder" || meshFilter.sharedMesh.name == "Cube";
				if (!cylinderOrCube)
				{
				}
				else if (trans.name == "HumppaKuutio")
				{
					SDFMissionStuff missionStuff = new SDFMissionStuff
					{
						pos = camRotInv * (trans.position - camPos),
						size = trans.lossyScale,
						rot = Quaternion.Inverse(camRotInv * trans.rotation),
						color = new Vector3(0, 1, 1),
					};

					missionStuffs.Add(missionStuff);
					continue;
				}
				else if (trans.parent == null)
				{
				}
				else if (trans.parent.name == "Generator")
				{
					SDFMissionStuff missionStuff = new SDFMissionStuff
					{
						pos = camRotInv * (trans.position - camPos),
						size = trans.lossyScale,
						rot = Quaternion.Inverse(camRotInv * trans.rotation),
						color = new Vector3(1, 0, 0),
					};

					missionStuffs.Add(missionStuff);
					continue;
				}
				else if (trans.parent.name == "Pump")
				{
					SDFMissionStuff missionStuff = new SDFMissionStuff
					{
						pos = camRotInv * (trans.position - camPos),
						size = trans.lossyScale,
						rot = Quaternion.Inverse(camRotInv * trans.rotation),
						color = new Vector3(0, 0.3f, 1),
					};

					missionStuffs.Add(missionStuff);
					continue;
				}
				else if (trans.parent.name == "Battery")
				{
					SDFMissionStuff missionStuff = new SDFMissionStuff
					{
						pos = camRotInv * (trans.position - camPos),
						size = trans.lossyScale,
						rot = Quaternion.Inverse(camRotInv * trans.rotation),
						color = new Vector3(0, 1, 0.3f),
					};

					missionStuffs.Add(missionStuff);
					continue;
				}
				else if (trans.parent.parent == null)
				{
				}
				else if (trans.parent.parent.name == "Generator")
				{
					SDFMissionStuff missionStuff = new SDFMissionStuff
					{
						pos = camRotInv * (trans.position - camPos),
						size = trans.lossyScale,
						rot = Quaternion.Inverse(camRotInv * trans.rotation),
						color = new Vector3(1, 0, 0),
					};

					missionStuffs.Add(missionStuff);
					continue;
				}
				else if (trans.parent.parent.name == "Pump")
				{
					SDFMissionStuff missionStuff = new SDFMissionStuff
					{
						pos = camRotInv * (trans.position - camPos),
						size = trans.lossyScale,
						rot = Quaternion.Inverse(camRotInv * trans.rotation),
						color = new Vector3(0, 0.3f, 1),
					};

					missionStuffs.Add(missionStuff);
					continue;
				}
				else if (trans.parent.parent.name == "Roller")
				{
					SDFMissionStuff missionStuff = new SDFMissionStuff
					{
						pos = camRotInv * (trans.position - camPos),
						size = trans.lossyScale,
						rot = Quaternion.Inverse(camRotInv * trans.rotation),
						color = new Vector3(1, 1, 0),
					};

					missionStuffs.Add(missionStuff);
					continue;
				}
				else if (trans.parent.parent.name == "Battery")
				{
					SDFMissionStuff missionStuff = new SDFMissionStuff
					{
						pos = camRotInv * (trans.position - camPos),
						size = trans.lossyScale,
						rot = Quaternion.Inverse(camRotInv * trans.rotation),
						color = new Vector3(0, 1, 0.3f),
					};

					missionStuffs.Add(missionStuff);
					continue;
				}

				if (trans.GetComponent<PlayerController>() != null)
				{
					Color c = trans.GetComponent<Renderer>().material.color;

					SDFMissionStuff missionStuff = new SDFMissionStuff
					{
						pos = camRotInv * (trans.position - camPos),
						size = trans.lossyScale,
						rot = Quaternion.Inverse(camRotInv * trans.rotation),
						color = new Vector3(c.r, c.g, c.b),
					};

					missionStuffs.Add(missionStuff);
					continue;
				}

				if (trans.parent != null)
				{
					if (trans.parent.name == "Wall2")
						continue;

					if (trans.parent.name == "Floor2")
						continue;
				}

				if (meshFilter.sharedMesh.name == "Cube")
				{
					Vector3 s = trans.lossyScale;
					s.y *= 1.5f;

					SDFBox box = new SDFBox
					{
						pos = camRotInv * (trans.position - camPos),
						size = trans.lossyScale,
						rot = Quaternion.Inverse(camRotInv * trans.rotation)
					};

					boxes.Add(box);
				}
				else if (meshFilter.sharedMesh.name == "Cylinder")
				{
					Vector3 s = trans.lossyScale;
					s.x *= 0.5f;
					s.y *= 2.0f;
					s.z *= 0.5f;

					SDFBox box = new SDFBox
					{
						pos = camRotInv * (trans.position - camPos),
						size = s,
						rot =  Quaternion.Inverse(camRotInv * trans.rotation)
					};

					boxes.Add(box);
				}
				else if (meshFilter.sharedMesh.name == "Sphere")
				{
					SDFSphere sphere = new SDFSphere
					{
						pos = camRotInv * (trans.position - camPos),
						radius = Mathf.Max(trans.lossyScale.x, 0.3f)
					};
					spheres.Add(sphere);
				}
			}
		}

		// Lights
		{
			Object[] objs = FindObjectsOfType<Light>();
			for (uint i = 0; i < objs.Length; ++i)
			{
				Light lightComp = (Light)objs[i];
				Transform trans = lightComp.transform;

				if (lightComp.type == LightType.Point && lightComp.intensity > 0.0)
				{
					SphereLight light = new SphereLight
					{
						pos = camRotInv * (trans.position - camPos),
						color = new Vector3(lightComp.color.r, lightComp.color.g, lightComp.color.b),
						intensity = lightComp.intensity
					};

					lights.Add(light);
				}
			}
		}
	}
}
