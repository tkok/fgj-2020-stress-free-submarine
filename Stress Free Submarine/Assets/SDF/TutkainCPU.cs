﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutkainCPU : MonoBehaviour
{
	public SceneNuuskutin nuuskutin;
	public ComputeShader tutkainCS;
	public Material tutkainMaterial;

	private RenderTexture tutkainTex;
	private RenderTexture lightingResultTex;
	private RenderTexture rendausTex;

	private const uint tutkainTexSizeX = 20 * 96 / 6;
	private const uint tutkainTexSizeY = 11 * 96 / 6;
	private const int shapesCountMax = 1024;

	private const uint renduasTexSizeX = tutkainTexSizeX;
	private const uint renduasTexSizeY = tutkainTexSizeY;

	List<SDFBox> boxes;
	List<SDFSphere> spheres;
	List<SphereLight> lights;
	List<SDFMissionStuff> missionStuffs;

	ComputeBuffer sphereBuffer;
	ComputeBuffer boxBuffer;
	ComputeBuffer lightsBuffer;
	ComputeBuffer missionStuffsBuffer;

	public float happoisuusMun = -1;
	public float syvyysMun = -1;

	void Start()
	{
		tutkainTex = new CustomRenderTexture((int)(tutkainTexSizeX), (int)(tutkainTexSizeY),
			RenderTextureFormat.ARGB32, RenderTextureReadWrite.Linear);
		tutkainTex.enableRandomWrite = true;

		lightingResultTex = new CustomRenderTexture((int)tutkainTexSizeX, (int)tutkainTexSizeY,
			RenderTextureFormat.ARGB32, RenderTextureReadWrite.Linear);
		lightingResultTex.enableRandomWrite = true;

		rendausTex = new CustomRenderTexture((int)(renduasTexSizeX), (int)(renduasTexSizeY),
			RenderTextureFormat.ARGB32, RenderTextureReadWrite.Linear);
		rendausTex.enableRandomWrite = true;

		boxes = new List<SDFBox>();
		spheres = new List<SDFSphere>();
		lights = new List<SphereLight>();
		missionStuffs = new List<SDFMissionStuff>();

		sphereBuffer = new ComputeBuffer(shapesCountMax, System.Runtime.InteropServices.Marshal.SizeOf<SDFSphere>());
		boxBuffer = new ComputeBuffer(shapesCountMax, System.Runtime.InteropServices.Marshal.SizeOf<SDFBox>());
		lightsBuffer = new ComputeBuffer(shapesCountMax, System.Runtime.InteropServices.Marshal.SizeOf<SphereLight>());
		missionStuffsBuffer = new ComputeBuffer(64, System.Runtime.InteropServices.Marshal.SizeOf<SDFMissionStuff>());
	}

	void Tutkain()
	{
		boxes.Clear();
		spheres.Clear();
		lights.Clear();
		missionStuffs.Clear();

		float camFov;
		float camNear;
		float camFar;

		nuuskutin.getVisible(out camFov, out camNear, out camFar, shapesCountMax
			, ref boxes, ref spheres, ref lights, ref missionStuffs);

		tutkainCS.SetInt("spheresCount", spheres.Count);
		sphereBuffer.SetData(spheres);

		tutkainCS.SetInt("boxesCount", boxes.Count);
		boxBuffer.SetData(boxes);

		tutkainCS.SetInt("missionStuffsCount", missionStuffs.Count);
		missionStuffsBuffer.SetData(missionStuffs);

		int kernelIndex = tutkainCS.FindKernel("CSMain");
		tutkainCS.SetTexture(kernelIndex, "Result", tutkainTex);
		tutkainCS.SetFloat("Scale", 10.0f);
		tutkainCS.SetFloat("Time", Time.time);
		tutkainCS.SetFloat("camFov", camFov);
		tutkainCS.SetFloat("camNear", camNear);
		tutkainCS.SetFloat("camFar", camFar);

		tutkainCS.SetFloat("happoisuus", TylsaaHappoo.happoisuus);
		tutkainCS.SetFloat("syvyys", TylsaaHappoo.syvyys);

		happoisuusMun = TylsaaHappoo.happoisuus;
		syvyysMun = TylsaaHappoo.syvyys;

		tutkainCS.SetBuffer(kernelIndex, "spheres", sphereBuffer);
		tutkainCS.SetBuffer(kernelIndex, "boxes", boxBuffer);
		tutkainCS.SetBuffer(kernelIndex, "missionStuffs", missionStuffsBuffer);
		uint threadGroupSizeX, threadGroupSizeY, threadGroupSizeZ;
		tutkainCS.GetKernelThreadGroupSizes(kernelIndex, out threadGroupSizeX, out threadGroupSizeY, out threadGroupSizeZ);

		int dispatchX = (int)(tutkainTexSizeX / threadGroupSizeX);
		int dispatchY = (int)(tutkainTexSizeY / threadGroupSizeY);
		int dispatchZ = (int)(1);

		tutkainCS.Dispatch(kernelIndex, dispatchX, dispatchY, dispatchZ);
	}

	void Lighting()
	{
		lightsBuffer.SetData(lights);

		int kernelIndex = tutkainCS.FindKernel("CSLighting");
		tutkainCS.SetTexture(kernelIndex, "LightingInput", tutkainTex);
		tutkainCS.SetTexture(kernelIndex, "LightingResult", lightingResultTex);

		tutkainCS.SetInt("lightsCount", lights.Count);
		tutkainCS.SetBuffer(kernelIndex, "lights", lightsBuffer);

		tutkainCS.SetBuffer(kernelIndex, "spheres", sphereBuffer);
		tutkainCS.SetBuffer(kernelIndex, "boxes", boxBuffer);
		tutkainCS.SetBuffer(kernelIndex, "missionStuffs", missionStuffsBuffer);

		uint threadGroupSizeX, threadGroupSizeY, threadGroupSizeZ;
		tutkainCS.GetKernelThreadGroupSizes(kernelIndex,
			out threadGroupSizeX, out threadGroupSizeY, out threadGroupSizeZ);

		int dispatchX = (int)(tutkainTexSizeX / threadGroupSizeX);
		int dispatchY = (int)(tutkainTexSizeY / threadGroupSizeY);
		int dispatchZ = (int)(1);

		tutkainCS.Dispatch(kernelIndex, dispatchX, dispatchY, dispatchZ);
	}

	void Rendaus()
	{
		int kernelIndex = tutkainCS.FindKernel("CSRendaus");
		tutkainCS.SetTexture(kernelIndex, "RendausInput", lightingResultTex);
		tutkainCS.SetTexture(kernelIndex, "RendausResult", rendausTex);
		tutkainCS.SetBuffer(kernelIndex, "missionStuffs", missionStuffsBuffer);

		tutkainCS.SetVector("globalLightColor", Vector4.Lerp(new Vector4(1, 0, 0, 1), new Vector4(1, 1, 1, 1), BlackBoard.electricSystem.totalChargePercent / 0.01f));

		uint threadGroupSizeX, threadGroupSizeY, threadGroupSizeZ;
		tutkainCS.GetKernelThreadGroupSizes(kernelIndex, out threadGroupSizeX, out threadGroupSizeY, out threadGroupSizeZ);

		int dispatchX = (int)(renduasTexSizeX / threadGroupSizeX);
		int dispatchY = (int)(renduasTexSizeY / threadGroupSizeY);
		int dispatchZ = (int)(1);
		tutkainCS.Dispatch(kernelIndex, dispatchX, dispatchY, dispatchZ);

		tutkainMaterial.mainTexture = rendausTex;
	}

	public void PreRendaaaaa()
	{
		Tutkain();
		Lighting();
		Rendaus();
	}

    private void OnDestroy()
	{
		if (sphereBuffer != null)
		{
			sphereBuffer.Release();
			sphereBuffer.Dispose();
		}

		if (boxBuffer != null)
		{
			boxBuffer.Release();
			boxBuffer.Dispose();
		}

		if (lightsBuffer != null)
		{
			lightsBuffer.Release();
			lightsBuffer.Dispose();
		}

		if (missionStuffsBuffer != null)
		{
			missionStuffsBuffer.Release();
			missionStuffsBuffer.Dispose();
		}
	}
}
