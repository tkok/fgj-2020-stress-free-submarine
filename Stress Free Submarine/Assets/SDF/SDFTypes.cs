﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct SDFBox
{
	public Vector3 pos;
	public Vector3 size;
	public Quaternion rot;
}

public struct SDFSphere
{
	public Vector3 pos;
	public float radius;
}

public struct SphereLight
{
	public Vector3 pos;
	public Vector3 color;
	public float intensity;
}

//public struct SDFPlayer
//{
//	public Vector3 pos;
//	public Vector3 size;
//	public Vector3 color;
//	public Quaternion rot;
//}

public struct SDFMissionStuff
{
	public Vector3 pos;
	public Vector3 size;
	public Vector3 color;
	public Quaternion rot;
}
