﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectricSystem : MonoBehaviour
{
    public List<Generator> powerGenerators = new List<Generator>(); 
    public List<PowerConsumer> powerConsumers = new List<PowerConsumer>(); 
    public List<Battery> powerStorages = new List<Battery>();

    public Material electicMaterial;
    public Color okColor;
    public Color badColor;

    public float totalChargePercent = 0;

    private void Awake()
    {
        BlackBoard.electricSystem = this;
    }
    private void Start()
    {
        PowerConsumer[] tempConsumers = FindObjectsOfType<PowerConsumer>();
        for (int i = 0; i < tempConsumers.Length; ++i)
        {
            if (!this.powerConsumers.Contains(tempConsumers[i]))
            {
                this.powerConsumers.Add(tempConsumers[i]);
            }
        }
    }
    // Update is called once per frame
    void Update()
    {
        float newPower = 0;
        for(int i = 0; i < powerGenerators.Count; ++i)
        {
            newPower += powerGenerators[i].production * Time.deltaTime;
        }
        float allPower = newPower;
        float neededPower = 0;
        for (int i = 0; i < powerStorages.Count; ++i)
        {
            allPower += powerStorages[i].charge;
        }

        for (int i = 0; i < powerConsumers.Count; ++i)
        {
            neededPower += powerConsumers[i].powerNeed * Time.deltaTime;
        }

        float t = Mathf.Clamp01(allPower / neededPower);

        for (int i = 0; i < powerConsumers.Count; ++i)
        {
            allPower -= powerConsumers[i].Consume(allPower, t);
        }

        float totalCapacity = 0;
        float totalCharge = 0;
        for (int i = 0; i < powerStorages.Count; ++i)
        {
            allPower -= powerStorages[i].SetCharge(allPower);
            totalCapacity += powerStorages[i].capacity;
            totalCharge += powerStorages[i].charge;
        }
        electicMaterial.SetColor("_EmissionColor", Color.Lerp(badColor, okColor, totalCharge / totalCapacity));
        this.totalChargePercent = totalCharge / totalCapacity;
    }
    private void OnDisable()
    {
        electicMaterial.SetColor("_EmissionColor",okColor);
    }
}
