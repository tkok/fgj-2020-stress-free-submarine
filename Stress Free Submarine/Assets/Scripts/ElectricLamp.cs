﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectricLamp : MonoBehaviour
{
    private new Light light;
    public float targetIntensity = 50;
    public float minimiIntensity = 0.1f;
    private PowerConsumer powerConsumer;

    public bool emergencyLamp = true;

    private void Start()
    {
        light = GetComponent<Light>();
        powerConsumer = GetComponent<PowerConsumer>();
    }

    // Update is called once per frame
    void LateUpdate()
    {
        float intensity = (targetIntensity - minimiIntensity) * powerConsumer.powerSatisfied;
        if (emergencyLamp && intensity < 0.1f)
        {
            light.intensity = 2 + 0.5f * Mathf.Sin(Time.time);
            light.color = Color.red;
        }
        else {
            if (emergencyLamp) {
                light.color = Color.white;
            }
            light.intensity = minimiIntensity + intensity;
        }
    }
}
