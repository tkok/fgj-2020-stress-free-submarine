﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Loss : MonoBehaviour
{
    bool loosing;
    float lossTime;
    float duration = 3;
    public GameObject button;
    public UnityEngine.UI.Image image;

    public void GameLoss()
    {
        loosing = true;
        lossTime = Time.time;
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }
        if (loosing)
        {
            float t = (Time.time - lossTime) / duration;
            if (t > 1)
            {
                t = 1;
                button.SetActive(true);
            }
            Color c = image.color;
            c.a = t;
            image.color = c;
        }
    }

    public void MainMenu()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }
}
