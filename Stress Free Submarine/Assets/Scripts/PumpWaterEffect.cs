﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PumpWaterEffect : MonoBehaviour
{
    public Vector3 startPos;
    public Vector3 endPos;
    public float speed = 2;
    public PumpHandler pumpHandler;

    private float currentPostion = 0;

    // Update is called once per frame
    void Update()
    {
        currentPostion += pumpHandler.speed * Time.deltaTime;
        currentPostion %= speed;

        transform.localPosition = Vector3.Lerp(startPos, endPos, currentPostion / speed);
    }
}
