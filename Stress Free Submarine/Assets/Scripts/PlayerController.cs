﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{

    public int playerNumber;

    public float speed;

    public float minVelocitySpeed;
    //SMB speeds
    private static float MIN_VELOCITY = 304.0f / 10496.0f;
    private static float RUN_ACCELERATION = 228.0f / 10496.0f;
    private static float RELEASE_DECELERATION = 208.0f / 10496.0f;
    private static float MAX_RUNNING_SPEED = 10496.0f / 10496.0f;

    public int? playerSlot = null;

    private int released = 2;

    private enum RELEASE_STATE {
        RELEASED,
        START_MOVING,
        STOP_MOVING,
        MOVING
    };

    private RELEASE_STATE handleRelease(float x, float y) {
        RELEASE_STATE result;
        if(Mathf.Abs(x) >= 1e-6 || Mathf.Abs(y) >= 1e-6) {
            if(this.released >= 2) {
                result = RELEASE_STATE.START_MOVING;
            } else {
                result = RELEASE_STATE.MOVING;
            }
            this.released = 0;
        } else {
            if(this.released >= 2) {
                result = RELEASE_STATE.RELEASED;
            } else {
                result = RELEASE_STATE.STOP_MOVING;
            }
            this.released += 1;
            if(this.released >= 2) {
                this.released = 2;
            }
        }
        return result;
    }

    private Material material;

    private Rigidbody rb;
    //INITIAL pi, use the one in BlackBoard.playerSystem.inputMap[playerSlot];
    private PlayerInput pi;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        pi = GetComponent<PlayerInput>();
        material = GetComponent<Renderer>().material;
        if(pi != null && pi.inputIsActive && pi.devices.Count != 0 && (BlackBoard.allowKeyboard || !pi.devices[0].name.Equals("Keyboard"))) {
            this.playerSlot = BlackBoard.playerSystem.inputMap.Count;
            BlackBoard.playerSystem.inputMap.Add(this.playerSlot.Value,pi);
            //print("Activate player slot " + this.playerSlot.Value);
        } else {
            BlackBoard.playerSystem.inactives.Add(this);
        }
        SetColor();
    }

    public void SetColor() {
        if(playerSlot.HasValue) {
            material.color = BlackBoard.playerSystem.playerColors[playerSlot.Value % BlackBoard.playerSystem.playerColors.Count];
        } else {
            material.color = BlackBoard.playerSystem.inactiveColor;
        }
    }

    private void SwapPlayer(bool forward) {
        if(!this.playerSlot.HasValue || BlackBoard.playerSystem.inactives.Count == 0) {
            return;
        }
        int currentSlot = this.playerSlot.Value;
        this.playerSlot = null;
        PlayerController replacement;
        int index;
        int insertIndex;
        if(forward) {
            index = 0;
            insertIndex = BlackBoard.playerSystem.inactives.Count-1;
        } else {
            index = BlackBoard.playerSystem.inactives.Count-1;
            insertIndex = 0;
        }
        replacement = BlackBoard.playerSystem.inactives[index];
        BlackBoard.playerSystem.inactives.RemoveAt(index);
        
        replacement.playerSlot = currentSlot;
        BlackBoard.playerSystem.inactives.Insert(insertIndex, this);
        this.swapCooldown = Time.time;
        replacement.swapCooldown = Time.time;
        this.SetColor();
        replacement.SetColor();
    }

    void FixedUpdate()
    {
        Vector2 move;
        if(!this.playerSlot.HasValue) {
            move = new Vector2(0.0f,0.0f);
        } else {
            move = BlackBoard.playerSystem.inputMap[this.playerSlot.Value].actions["move"].ReadValue<Vector2>();
        }
        float moveHorizontal = move.x;
        float moveVertical = move.y;
        float moveY = rb.velocity.y;

        RELEASE_STATE releaseState = handleRelease(moveHorizontal, moveVertical);

        if(releaseState == RELEASE_STATE.START_MOVING) {
            Vector2 startMotion = new Vector2(moveHorizontal, moveVertical);
            startMotion = startMotion.normalized * MIN_VELOCITY * minVelocitySpeed;
            rb.velocity = new Vector3(startMotion.x, moveY, startMotion.y);
        } else if(releaseState == RELEASE_STATE.STOP_MOVING) {
            Vector2 stopMotion = new Vector2(rb.velocity.x, rb.velocity.z);
            stopMotion = stopMotion.normalized * MAX_RUNNING_SPEED * speed;
            rb.velocity = new Vector3(stopMotion.x, moveY, stopMotion.y);
        } else {
            Vector2 currentMovement = new Vector2(rb.velocity.x, rb.velocity.z);
            Vector2 inputMovement = new Vector2(moveHorizontal, moveVertical);
            if(releaseState == RELEASE_STATE.RELEASED) {
                float motion = currentMovement.magnitude;
                motion -= RELEASE_DECELERATION * speed;
                if(motion <= 1e-6) {
                    motion = 0.0f;
                }
                Vector2 finalMovement = currentMovement.normalized * motion;
                rb.velocity = new Vector3(finalMovement.x, moveY, finalMovement.y);
            } else if(releaseState == RELEASE_STATE.MOVING) {
                float motion = currentMovement.magnitude;
                motion += inputMovement.magnitude*speed*RUN_ACCELERATION;
                if(motion >= MAX_RUNNING_SPEED * speed) {
                    motion = MAX_RUNNING_SPEED * speed;
                }
                Vector2 finalMovement;
                if(inputMovement.magnitude <= 0.05) {
                    finalMovement = currentMovement.normalized * motion;
                } else {
                    finalMovement = inputMovement.normalized * motion;
                }
                rb.velocity = new Vector3(finalMovement.x, moveY, finalMovement.y);
            }
        }
        handleSwap();
    }

    public float swapCooldown = 0.0f;

    private void handleSwap() {
        if(Time.time - swapCooldown <= 1.0f) {
            return;
        }
        if(!this.playerSlot.HasValue) {
            return;
        }
        PlayerInput currentInput = BlackBoard.playerSystem.inputMap[this.playerSlot.Value];

        bool next = currentInput.actions["next"].ReadValue<float>() >= 0.5;
        bool previous = currentInput.actions["previous"].ReadValue<float>() >= 0.5;
        if(next && previous) {
            return;
        }
        if(next) {
            SwapPlayer(true);
        } else if(previous) {
            SwapPlayer(false);
        }
    }
}
    