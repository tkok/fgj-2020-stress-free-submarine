﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerLevelIndicator : MonoBehaviour
{
    public Battery battery;
    public Transform target;

    private Vector3 startScale;

    private void Start()
    {
        startScale = target.localScale;
        
    }
    // Update is called once per frame
    void LateUpdate()
    {
        target.localScale = new Vector3(startScale.x, battery.charge / battery.capacity, startScale.z);
    }
}
