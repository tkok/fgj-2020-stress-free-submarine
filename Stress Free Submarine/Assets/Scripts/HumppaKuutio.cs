﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumppaKuutio : MonoBehaviour
{
    public PowerConsumer consumer;
    public AudioSource audioSource;

    public AnimationCurve xScale;
    public AnimationCurve yScale;
    public AnimationCurve zScale;

    public float avgScale = 0.7f;
    public float curveScale = 0.7f;

    public float scaleTime = 1;
    private float currentTime = 0;

    void Start()
    {
        consumer.powerNeed = 1;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        audioSource.volume = Mathf.Lerp(audioSource.volume, consumer.powerSatisfied, Time.deltaTime);
        audioSource.pitch = audioSource.volume;
        currentTime += Time.deltaTime * audioSource.volume;
        currentTime = currentTime % scaleTime;
        float t = Mathf.Clamp01(currentTime / scaleTime);
        transform.localScale = Vector3.one * avgScale + new Vector3(xScale.Evaluate(t),yScale.Evaluate(t),zScale.Evaluate(t)) * curveScale;
    }
}
