﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UseBalls : MonoBehaviour
{
    public GameObject useBallPrefab;
    public List<UseBall> useBalls = new List<UseBall>();

    private void Awake()
    {
        BlackBoard.useBalls = this;
    }

    public UseBall SendNewBall(Transform target, Vector3 startPos)
    {
        for(int i = 0; i < this.useBalls.Count; ++i)
        {
            if (!this.useBalls[i].inUse)
            {
                return this.useBalls[i].Init(target, startPos);
            }
        }

        GameObject newBall = Instantiate<GameObject>(useBallPrefab);
        UseBall newBallComponent = newBall.GetComponent<UseBall>();
        useBalls.Add(newBallComponent);
        return newBallComponent.Init(target, startPos);
    }
}
