﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialFinder : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        if (!Application.isEditor)
        {
            return;
        }
        MeshRenderer[] renderers = FindObjectsOfType<MeshRenderer>();
        List<string> results = new List<string>();
        for(int i =0; i < renderers.Length; ++i)
        {
            if (!results.Contains(renderers[i].material.name))
            {
                Debug.Log("New material " + renderers[i].material.name);
                results.Add(renderers[i].material.name);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
