﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Battery : MonoBehaviour
{
    public float capacity = 10;
    public float charge = 10;
    public float selfDischarge = 0.1f;

    internal float SetCharge(float newPower)
    {
        charge = Mathf.Min(capacity, newPower);
        float result = charge;
        charge = Mathf.Max(0, charge - selfDischarge * Time.deltaTime); 
        return result;
    }
}
