﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class BlackBoard
{
    public static UseBalls useBalls;
    public static FloorParent floorParent;
    public static TheGame theGame;
    public static PlayerSystem playerSystem;
    public static ElectricSystem electricSystem;

    public static bool useFullSDF;
    public static bool allowKeyboard = true;
}
