﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Motor : MonoBehaviour
{
    public Transform roller;
    public float idleSpeed = 10;
    public float useSpeed = 30;

    public float productionInUse = 10;
    public float productionInIdle = 0;

    public PowerConsumer powerConsumer;

    private void Start()
    {
        if (this.powerConsumer == null)
        {
            this.powerConsumer = gameObject.AddComponent<PowerConsumer>();
        }
    }

    void LateUpdate()
    {
        roller.localEulerAngles = new Vector3(0, 90, Mathf.Lerp(idleSpeed, useSpeed, this.powerConsumer.powerSatisfied) * Time.time);
    }
}