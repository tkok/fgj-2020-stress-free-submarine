﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pump : MonoBehaviour
{
    public PlayerProximitySensor playerProximitySensor;
    public float pumpPower = 5;

    internal float GetPumpPower()
    {
        return playerProximitySensor .playerCount * pumpPower;
    }
}
