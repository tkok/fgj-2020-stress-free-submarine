﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PumpHandler : MonoBehaviour
{

    public Vector3 endPos;
    public Vector3 startPos;

    public AnimationCurve pumpCurve;
    private float currentValue;
    public float speed = 0;
    public float lerpMultiplier = 2;

    public PlayerProximitySensor playerProximitySensor;

    // Update is called once per frame
    void Update()
    {
        speed = Mathf.Lerp(speed, playerProximitySensor.playerCount, lerpMultiplier * Time.deltaTime);
        currentValue += speed * Time.deltaTime;
        currentValue %= 1f;
        transform.localPosition = Vector3.Lerp(startPos, endPos, pumpCurve.Evaluate(currentValue));
    }
}
