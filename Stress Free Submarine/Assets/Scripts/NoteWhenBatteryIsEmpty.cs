﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteWhenBatteryIsEmpty : MonoBehaviour
{
    public TMPro.TextMeshPro label;

    public bool water = false;

    public float tresshold = 0.2f;
    // Start is called before the first frame update
    void Start()
    {
        label.text = water ? "P" : "G";
    }

    // Update is called once per frame
    void Update()
    {
        if( water ? BlackBoard.theGame.waterPercent > tresshold : BlackBoard.electricSystem.totalChargePercent < tresshold )
        {
            label.text = "!";
            transform.localPosition = new Vector3(0.01f, 1.3f, 0.04f + 0.08f * Mathf.Sin(Time.time * 0.5f));
            label.color = Color.red;
        }
        else
        {
            label.text = water ? "P" : "G";
            label.color = Color.black;
        }
    }
}
