﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Leak : MonoBehaviour
{
    public new ParticleSystem particleSystem;
    public float startSeverity = 1;
    public float currentSeverity;
    public float maxSeverity = 10;
    public float getWorstSpeed = 0.5f;
    public float fixingSpeed = 0.25f;
    public float sizeMultiplier = 0.5f;
    public float baseMultiplier = 0.1f;
    public float emissionMultiplier = 3f;

    public GameObject warnObject;

    public float baseHealth = 5;

    private bool isFixed = false;

    public PlayerProximitySensor playerProximitySensor;

    private void Start()
    {
        currentSeverity = startSeverity;
        warnObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (isFixed)
        {
            if(particleSystem.particleCount <= 2)
            {
                Destroy(gameObject);
            }
            return;
        }
        if (playerProximitySensor.playerCount > 0)
        {
            baseHealth -= fixingSpeed * Time.deltaTime;
            if (baseHealth < 0) { 
                currentSeverity -= fixingSpeed * Time.deltaTime;
            }
        }
        else
        {
            currentSeverity += getWorstSpeed * Time.deltaTime;
        }
        currentSeverity = Mathf.Min(maxSeverity, currentSeverity);
        if(currentSeverity < 0)
        {
            BlackBoard.theGame.RemoveLeak(this);
            playerProximitySensor.enabled = false;
            isFixed = true;
        }
        if (currentSeverity > maxSeverity * 0.7f)
        {
            warnObject.SetActive(true);
        }
        else
        {
            warnObject.SetActive(false);
        }
        transform.localScale = Vector3.one * sizeMultiplier * (baseMultiplier+currentSeverity);
        var emission = particleSystem.emission;
        emission.rateOverTime = currentSeverity * emissionMultiplier;
    }


}
