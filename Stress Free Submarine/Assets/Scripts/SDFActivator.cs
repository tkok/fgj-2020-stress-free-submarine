﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SDFActivator : MonoBehaviour
{
    public List<GameObject> activateWhenSDF = new List<GameObject>();

    private void Awake()
    {
        for(int i = 0; i < activateWhenSDF.Count; ++i)
        {
            activateWhenSDF[i].SetActive(BlackBoard.useFullSDF);
        }
    }
}
