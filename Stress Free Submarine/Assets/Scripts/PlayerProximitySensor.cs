﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerProximitySensor : MonoBehaviour
{
    public float distance = 2;

    private int skipFrames = 9;
    private int frame = 0;
    private int ballSkipTimes = 1;
    private int ballFrame;


    public int playerCount = 0;

    private void Update()
    {
        ++frame;
        if(frame <= skipFrames)
        {
            return;
        }
        frame = 0;
        float distanceSqr = distance * distance;
        Vector3 position = transform.position;
        ++ballFrame;
        playerCount = 0;
        for (int i = 0; i < BlackBoard.theGame.players.Count; ++i)
        {
            if ((BlackBoard.theGame.players[i].transform.position - position).sqrMagnitude < distanceSqr)
            {
                playerCount++;
                if(ballFrame > ballSkipTimes)
                {
                    BlackBoard.useBalls.SendNewBall(transform, BlackBoard.theGame.players[i].transform.position);
                }
            }
        }
        if (ballFrame > ballSkipTimes)
        {
            ballFrame = 0;
        }
        
    }

}
