﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerSystem : MonoBehaviour
{
    [HeaderAttribute("Players")]
    public Dictionary<int, PlayerInput> inputMap = new Dictionary<int,PlayerInput>();
    public List<PlayerController> players = new List<PlayerController>();
    public List<PlayerController> inactives = new List<PlayerController>();
    public List<Color> playerColors = new List<Color>();
    public Color inactiveColor;

    public int ActivePlayers {
        get {
            return inputMap.Count;
        }
    }

    // Start is called before the first frame update
    void Awake()
    {
        playerColors.Add(new Color(0.0f,1.0f,0.0f));
        playerColors.Add(new Color(1.0f,0.0f,1.0f));
        playerColors.Add(new Color(0.0f,0.0f,1.0f));
        playerColors.Add(new Color(1.0f,1.0f,0.0f));
        playerColors.Add(new Color(1.0f,0.0f,0.0f));
        playerColors.Add(new Color(0.0f,1.0f,1.0f));
        inactiveColor = new Color(0.66f, 0.66f, 0.66f);

        BlackBoard.playerSystem = this;
        players = new List<PlayerController>(FindObjectsOfType<PlayerController>());

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
