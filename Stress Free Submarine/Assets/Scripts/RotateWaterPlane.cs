﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateWaterPlane : MonoBehaviour
{
    public float rotation = 2.5f;
    public float speed = 3;

    // Update is called once per frame
    void Update()
    {
        transform.localEulerAngles = new Vector3(0, 0, Mathf.Sin(speed * Time.time) * rotation);
    }
}
