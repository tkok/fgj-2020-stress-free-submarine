﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TheGame : MonoBehaviour
{
    [Header("Leaks")]
    public GameObject leakPrefab;
    private List<Leak> leaks = new List<Leak>();
    public List<LeakSpawn> spawnInterval = new List<LeakSpawn>();
    [Serializable]
    public class LeakSpawn
    {
        public float depth;
        public float amount;
        [NonSerialized]
        public bool added = false;
    }
    public float waterPercent;
    private float spawnMinDistanceSqr = 4;
    public Transform waterPlane;

    public Material floorMaterial;

    // Water
    private float waterLevel = 0;
    private float maxWaterLevel = 50;
    public float waterLeakSpeed = 1;
    public Pump thePump;

    public TMPro.TextMeshProUGUI infoLabel;
    public Loss loss;

    // Depth
    private float depth = 3000f;
    private float riseSpeed = 1000f/60f; // 1000 m per 60 s

    [Header("Players")]
    public List<PlayerController> players = new List<PlayerController>();
    public GameObject playerPrefab;
    public Vector3 newPlayerPostion = new Vector3(5, 5, -5);

    [Header("Motor")]
    public PowerConsumer motorPower;

    public bool loosing = false;

    private void Awake()
    {
        BlackBoard.theGame = this;
        players = new List<PlayerController>(FindObjectsOfType<PlayerController>());
        Application.targetFrameRate = 60;
    }

    public void NewPlayer()
    {
        GameObject newPlayerGameObject = Instantiate<GameObject>(playerPrefab, newPlayerPostion, Quaternion.identity);
        players.Add(newPlayerGameObject.GetComponent<PlayerController>());
    }

    internal void RemoveLeak(Leak leak)
    {
        this.leaks.Remove(leak);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }
        for (int i = 0; i < spawnInterval.Count; ++i)
        {
            if(spawnInterval[i].added || spawnInterval[i].depth < depth)
            {
                continue;
            }
            spawnInterval[i].added = true;
            for(int l = 0; l < spawnInterval[i].amount; ++l)
            {
                int count = 10;
                while (--count > 0)
                {
                    Vector3 pos = BlackBoard.floorParent.GetRandomFloorPosition();
                    bool ok = true;
                    for (int j = 0; j < leaks.Count; ++j)
                    {
                        if ((pos - leaks[j].transform.position).sqrMagnitude < spawnMinDistanceSqr)
                        {
                            ok = false;
                            break;
                        }
                    }

                    if (ok)
                    {
                        GameObject newLeak = Instantiate<GameObject>(leakPrefab);
                        this.leaks.Add(newLeak.GetComponent<Leak>());
                        newLeak.transform.position = pos;
                        break;
                    }
                }
            }
        }

        // Water system
        for(int i= 0; i < leaks.Count; ++i)
        {
            this.waterLevel += Time.deltaTime * waterLeakSpeed;    
        }
        this.waterLevel -= (thePump.GetPumpPower() / (1 + leaks.Count)) * Time.deltaTime;
        this.waterLevel = Mathf.Max(0, this.waterLevel);
        waterPercent = Mathf.Sqrt(Mathf.Clamp01(waterLevel / maxWaterLevel));

        // first 10 % change to blue
        this.floorMaterial.color = new Color(Mathf.Clamp01(1 - (waterPercent/0.1f)), Mathf.Clamp01(1 - (waterPercent/0.1f)), 1);
        //
        if(waterPercent < 0.1f)
        {
            waterPlane.position = new Vector3(0, -0.2f, 0);
        }
        else
        {
            waterPlane.position = new Vector3(0, -0.1f + 0.99f * (Mathf.Sqrt(waterPercent) - 0.1f / 0.9f), 0);
        }

        TylsaaHappoo.happoisuus = 0.2f + (this.waterPercent / 1f);
        TylsaaHappoo.syvyys = depth / 3000f;

        depth += -(riseSpeed * motorPower.powerSatisfied * (1-(waterPercent * waterPercent))) * Time.deltaTime;

        if (depth < 0) {
            infoLabel.text = "You are stress free!";
            loss.GameLoss();
            loosing = true;
            enabled = false;
            return;
        }
        if (waterLevel > maxWaterLevel)
        {
            infoLabel.text = "Don't stress! Water level: 99 %\nDepth " + depth.ToString("0")+ "";
            // loss.GameLoss();
            // loosing = true;
            // enabled = false;
        }
        else
        {
            infoLabel.text = "Water level: " + (Mathf.Min(0.99f, waterPercent)).ToString("0 %") + "\nDepth " + depth.ToString("0")+ "";
        }
    }

    private void OnDisable()
    {
        floorMaterial.color = Color.white;
    }
}
