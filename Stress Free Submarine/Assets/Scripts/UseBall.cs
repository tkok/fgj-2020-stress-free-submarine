﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UseBall : MonoBehaviour
{
    public Vector3 target;
    public float duration = 1f;
    public bool inUse = false;
    private float startTime;
    private Vector3 startPos;
    private float speed = 1.8f;

    // Start is called before the first frame update
    public UseBall Init(Transform target, Vector3 startPos)
    {
        inUse = true;
        startTime = Time.time;
        this.target = target.position;
        transform.position = startPos;
        this.startPos = startPos;
        gameObject.SetActive(true);

        duration = (this.target - startPos).magnitude / speed;
        if(duration == 0)
        {
            duration = 0.1f;
        }

        return this;
    }

    // Update is called once per frame
    void Update()
    {
        float time = Time.time - startTime;
        time = time / duration;
        if(time > 1)
        {
            time = 1;
            inUse = false;
            gameObject.SetActive(false);
        }
        transform.position = Vector3.Lerp(startPos, target, time);

    }
}
