﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorParent : MonoBehaviour
{

    private List<FloodFloor> floorTiles = new List<FloodFloor>();
    // Start is called before the first frame update

    void Awake()
    {
        BlackBoard.floorParent = this;
    }

    void Start()
    {
        transform.GetComponentsInChildren<FloodFloor>(floorTiles);
    }

    internal Vector3 GetRandomFloorPosition()
    {
        return floorTiles[Random.Range(0, floorTiles.Count)].transform.position + new Vector3(0,0.5f,0);
    }
}
