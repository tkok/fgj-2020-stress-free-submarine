﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public Toggle allowKeyboard;
    public Toggle useSDF;

    public void StartGame() {
        BlackBoard.useFullSDF = useSDF.isOn;
        BlackBoard.allowKeyboard = allowKeyboard.isOn;
        print("Starting game");
        if(BlackBoard.useFullSDF) {
            SceneManager.LoadScene("MainScene");
        } else {
            SceneManager.LoadScene("Submarine");
        }
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
}
