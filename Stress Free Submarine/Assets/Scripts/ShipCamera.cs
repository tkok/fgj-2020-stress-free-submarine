﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipCamera : MonoBehaviour
{

    Vector3 startPos;
    private void Start()
    {
        this.startPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = startPos +
            new Vector3(
                Mathf.Sin(0.25f* Time.time + 0.25f * Mathf.PI)* 0.8f,
                Mathf.Cos(Time.time * 0.5f) * 0.45f);
    }
}
