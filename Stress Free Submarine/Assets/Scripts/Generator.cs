﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generator : MonoBehaviour
{
    public Transform roller;
    public float idleSpeed = 10;
    public float useSpeed = 30;
    public float production;

    public float productionInUse = 10;
    public float productionInIdle = 0;

    public PlayerProximitySensor playerProximitySensor;

    private void Start()
    {
        if(this.playerProximitySensor == null)
        {
            this.playerProximitySensor = gameObject.AddComponent<PlayerProximitySensor>();
        }
    }

    void LateUpdate()
    {
        roller.localEulerAngles = new Vector3(0, 0, (this.playerProximitySensor.playerCount > 0 ? (this.playerProximitySensor.playerCount * useSpeed) : idleSpeed) * Time.time);
        production = this.playerProximitySensor.playerCount > 0 ? this.playerProximitySensor.playerCount * productionInUse : productionInIdle;
    }
}
