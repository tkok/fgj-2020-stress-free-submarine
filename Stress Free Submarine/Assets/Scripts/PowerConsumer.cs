﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerConsumer : MonoBehaviour
{
    public float powerNeed = 1;
    public float powerSatisfied = 1;

    internal float Consume(float power, float percent)
    {
        float maxSatisfaction = percent;

        float powerPerThisFrame = powerNeed * percent * Time.deltaTime;
        if(powerPerThisFrame == 0)
        {
            powerSatisfied = percent;
            return 0;
        }

        if(power > powerPerThisFrame)
        {
            powerSatisfied = maxSatisfaction;
            return powerPerThisFrame;
        }

        powerSatisfied = (power / powerPerThisFrame)  * percent;
        return power;
    }
}
